﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	
	Transform direc;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = Vector3.MoveTowards(this.transform.position, direc.position, 1f);
	}

	public void shot(GameObject direction) {
		direc = direction.transform;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag.Equals ("Player"))
			print("I was shot");
		Destroy(this.gameObject);
	}
}
