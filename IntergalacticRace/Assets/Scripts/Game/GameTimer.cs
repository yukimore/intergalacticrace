﻿using UnityEngine;
using System.Collections;

public class GameTimer : MonoBehaviour {

	public int minutes = 0;
	public float secs = 0;
	public int intSecs = 0;
	public string seconds;
	string time;
	
	void Start () {
		
	}
	
	void Update () {
		secs = secs + Time.deltaTime;
	}
	
	void OnGUI(){
		
		intSecs = (int)secs;
		
		if(intSecs < 10){
			seconds = "0"+intSecs.ToString();
		}else{
			seconds = intSecs.ToString();
		}
		
		if(intSecs >= 60){
			minutes++;
			secs=0;
		}
		
		time = minutes.ToString() + ":" + seconds;
		GUI.Label (new Rect (1820, 50, 100, 100), time);
	}
}
