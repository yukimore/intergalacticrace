﻿using UnityEngine;
using System.Collections;

public class PauseQuestActivator : MonoBehaviour {

	public GameObject quest;

	void OnMouseDown(){
		quest.SetActive (true);
	}
}
