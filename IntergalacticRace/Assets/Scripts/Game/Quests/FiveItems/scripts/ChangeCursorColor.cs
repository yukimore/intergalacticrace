﻿using UnityEngine;
using System.Collections;

public class ChangeCursorColor : MonoBehaviour {

	//public GameObject cursor;

	
	void OnMouseEnter () {
		if(Vector3.Distance(transform.position,Camera.main.transform.position) < 20){
		GameObject.FindGameObjectWithTag("Cursor").GetComponent<GUITexture>().color = Color.blue;

			if(Vector3.Distance(transform.position,Camera.main.transform.position) < 10){
				GameObject.FindGameObjectWithTag("Cursor").GetComponent<GUITexture>().color = Color.green;
			}
		}
	}	

	void OnMouseExit () {
		if (GameObject.FindGameObjectWithTag ("Cursor") != null)
		GameObject.FindGameObjectWithTag ("Cursor").GetComponent<GUITexture>().color = Color.white;

	}

	void OnDestroy(){
		OnMouseExit ();
	}
}
