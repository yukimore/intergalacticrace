﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FiveItemsQuestController : MonoBehaviour {

	//real objects and list to check
	public GameObject[] FiveItemsQuestTag;
	private int[] FiveItemsQuestTagAccessed;
	public GameObject pauseController;

	//item to actvate the quest score system
	//public GameObject FiveItemsQuest;
	
	public int score; //score of the quest
	public int floor; //player's floor
	bool activated = false; //check if player already clicked to activate quest (prevent generation of multiple quests at the same time

	//score to appear on screen (slots and texture
	public GUITexture[] printedScore;
	public Texture[] printedScoreTextures;
	public GameObject victory;

	public int next;
	MazePlacer mazeplacer;

	void Start(){

		if (!activated){
			mazeplacer = GameObject.FindGameObjectWithTag("GameController").GetComponent<MazePlacer> ();
			mazeplacer.SetItemMaze(FiveItemsQuestTag,floor, "fiveItems");
			Invoke ("InitializeFiveItemsQuest", 1);
			activated = true;
			//FiveItemsQuest.SetActive (activated);
			gameObject.GetComponent<Timer> ().enabled = activated;
			gameObject.GetComponent<Timer> ().position = new Vector2(1870, 565);
			floor = pauseController.GetComponent<PauseGame>().floor;
		}
	}

	public void InitializeFiveItemsQuest()
	{
		FiveItemsQuestTag = GameObject.FindGameObjectsWithTag ("fiveItems");

		//changing tag to tagQuest Wrong
		for (int i = 0; i < FiveItemsQuestTag.Length; i++){
			FiveItemsQuestTag[i].tag = "wrong";
			FiveItemsQuestTag[i].AddComponent<BoxCollider>();
		}

		FiveItemsQuestTagAccessed = new int[FiveItemsQuestTag.Length];

		//initializing list of accessed items
		for (int i = 0; i < FiveItemsQuestTag.Length; i++){
			FiveItemsQuestTagAccessed[i] = -1;
		}

		FiveItemsQuestNextObject ();
	}

	//function to choose the next object
	public void FiveItemsQuestNextObject(){
		int count = 0;
		next = Random.Range (0, FiveItemsQuestTag.Length);
		while (count <6) {
			if (FiveItemsQuestTagAccessed [next] == -1) {
				FiveItemsQuestTagAccessed [next] = next;
				FiveItemsQuestTag [next].tag = "next";
				FiveItemsQuestTag [next].AddComponent<ChangeCursorColor> ();
				FiveItemsQuestTag [next].AddComponent<FiveItemsQuestNext> ();
				return;
			}else{
				next = Random.Range (0, FiveItemsQuestTag.Length);
			}
			count++;
		}
		if (count == 6)
			FiveItemsQuestFinalized(true);
	}


	void FiveItemsQuestFinalized(bool completed){
		for (int i = 0; i < FiveItemsQuestTag.Length; i++) {
			Destroy (FiveItemsQuestTag [i]);
		}
		victory.SetActive(true);

		if (completed) {
			GameObject.FindGameObjectWithTag("GameController").GetComponent<Timer>().minutes += 5;
		}

		Invoke ("resetQuest", 3);
	}

	void resetQuest(){
		activated = false;
		gameObject.GetComponent<Timer> ().minutes = 5;
		gameObject.GetComponent<Timer> ().seconds = 0;
		gameObject.GetComponent<Timer> ().enabled = activated;
		this.gameObject.SetActive (activated);
		victory.SetActive(false);
		//FiveItemsQuest.SetActive (activated);
	}

	void Update(){

		if (score == 0){
			for (int i = 0; i < score; i++){
				printedScore[i].texture = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().notUsed;
			}
		}
		if (score >=1)
			printedScore [score-1].texture = printedScoreTextures [score-1];
		if (gameObject.GetComponent<Timer> ().minutes == 0 && gameObject.GetComponent<Timer> ().seconds == 0) {
			FiveItemsQuestFinalized(false);
				}

	}
}
