﻿using UnityEngine;
using System.Collections;

public class FiveItemsQuestNext : MonoBehaviour {

	public GameObject controller;

	void OnMouseUpAsButton(){
		controller = GameObject.FindGameObjectWithTag ("FiveItemsQuest");
		if (transform.tag == "next" && GameObject.FindGameObjectWithTag("Cursor").GetComponent<GUITexture>().color == Color.green){
				transform.tag = "wrong";
				Destroy(gameObject.GetComponent<ChangeCursorColor>());
				controller.GetComponent<FiveItemsQuestController> ().score++;
		}else{
			controller.GetComponent<FiveItemsQuestController> ().score = 0;
		}
		controller.GetComponent<FiveItemsQuestController> ().FiveItemsQuestNextObject ();
		Destroy(gameObject.GetComponent<FiveItemsQuestNext>());
	}

}
