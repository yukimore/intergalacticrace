﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {

	public bool paused = false;
	public GameObject player;
	public GameObject PauseScreen;
	public int floor = 0;

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			paused = !paused;
			if (paused) {
				Transform cameraTransform = Camera.main.transform;
				transform.position = cameraTransform.position;
				transform.rotation = cameraTransform.rotation;
			}
			GetComponent<Camera>().enabled = paused;
			floor = player.GetComponent<playerStatus>().floor;
			player.SetActive (!paused);
			PauseScreen.SetActive (paused);
			if (paused)
				Time.timeScale = 0;
			else
				Time.timeScale = 1;
			//gameObject.GetComponent<FiveItemsQuestController>().floor = this.floor;
			}
			
		}
}
