﻿using UnityEngine;
using System.Collections;

public class Frame : MonoBehaviour {

	public GameObject element;

	
	// Update is called once per frame
	void Update () {
		this.GetComponent<GUITexture>().enabled = element.GetComponent<GUITexture>().enabled;
	
	}
}
