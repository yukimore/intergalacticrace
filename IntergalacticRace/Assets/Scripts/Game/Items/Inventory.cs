﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	public GameObject[] ExtraSlots; //inventory where items go
	public GameObject[] slots; //usable slots
	public string[] keys; //keyboad command to slot 
	GameObject gameController; 
	public Texture notUsed; //without functionality


	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("GameController");
	
		for (int i = 0; i<slots.Length; i++){ //gets the item selected on the previos menu
			slots[i].AddComponent<slot>();
			slots[i].GetComponent<slot>().MyInventory = this;
			slots[i].GetComponent<slot>().key = keys[i];
			slots[i].GetComponent<GUITexture>().texture = gameController.GetComponent<GameController>().slotsInitializer(slots[i].GetComponent<slot>().key);
		}

		for (int i= 0; i < ExtraSlots.Length; i++){
			ExtraSlots[i].GetComponent<GUITexture>().enabled = false;
		}
	}

	public void Take(ItemInventory item){
		for (int i = 0; i < ExtraSlots.Length; i++){
			if (ExtraSlots[i].GetComponent<GUITexture>().texture == notUsed){
				ExtraSlots[i].GetComponent<GUITexture>().texture = item.icon;
				Destroy(item.gameObject);
				break;
			}
		}
	}


}
