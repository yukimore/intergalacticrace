﻿#pragma strict

public var selectedPlayer : ThirdPersonController;

function OnTriggerEnter (other : Collider) {
 if (other.tag.Equals("Player")) {
  selectedPlayer = other.GetComponent(ThirdPersonController);
  selectedPlayer.runSpeed *= 2;
  selectedPlayer.walkSpeed *= 2;
  selectedPlayer.trotSpeed *= 2;
  Invoke("Normalize", 5f);
 }
}

function Normalize() {
 selectedPlayer.runSpeed /= 2;
 selectedPlayer.walkSpeed /= 2;
 selectedPlayer.trotSpeed /= 2;
 selectedPlayer = null;
}