﻿using UnityEngine;
using System.Collections;

public class ElevatorUpActivator : MonoBehaviour {

	GameObject player;
	public AudioSource startElevator;
	public AudioSource elevatorUp;

	void OnTriggerEnter (Collider colision){
		if (colision.gameObject.tag == "Player") {
			player = colision.gameObject;
			ElevatorUpChild[] children =gameObject.GetComponentsInChildren<ElevatorUpChild>();
			//startElevator.Play();
			foreach (ElevatorUpChild child in children){
				child.gameObject.GetComponent<BoxCollider>().enabled = true;
			}
			Invoke("ElevatorActivated", 5);
				}
		Invoke ("ElevatorReplacer", 7);
	}

	void ElevatorReplacer(){
		gameObject.GetComponent<ElevatorUp> ().targetA.GetComponent<Collider>().enabled = true;
		ElevatorUpChild[] children =gameObject.GetComponentsInChildren<ElevatorUpChild>();
		foreach (ElevatorUpChild child in children){
			child.gameObject.GetComponent<BoxCollider>().enabled = false;
		}
		gameObject.GetComponent<ElevatorUp>().enabled = false;
		gameObject.GetComponent<ElevatorUp> ().t = 0;
		gameObject.transform.position = gameObject.GetComponent<ElevatorUp> ().targetB.transform.position;


	}

	void ElevatorActivated(){
		//elevatorUp.Play ();
		gameObject.GetComponent<ElevatorUp>().enabled = true;
		gameObject.GetComponent<ElevatorUp>().targetA.GetComponent<Collider>().enabled = false;
		player.GetComponent<playerStatus> ().floor ++;
	}
}
