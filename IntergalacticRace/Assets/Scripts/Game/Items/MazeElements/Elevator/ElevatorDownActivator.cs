﻿using UnityEngine;
using System.Collections;

public class ElevatorDownActivator : MonoBehaviour {

	GameObject player; 

	void OnTriggerEnter (Collider colision){
		if (colision.gameObject.tag == "Player") {
			player = colision.gameObject;
			ElevatorDownChild[] children =gameObject.GetComponentsInChildren<ElevatorDownChild>();
			foreach (ElevatorDownChild child in children){
				child.gameObject.GetComponent<BoxCollider>().enabled = true;
			}
			Invoke("ElevatorActivated", 5);
				}
		Invoke ("ElevatorReplacer", 7);
	}

	void ElevatorReplacer(){
		gameObject.GetComponent<ElevatorDown> ().targetB.GetComponent<Collider>().enabled = true;
		ElevatorDownChild[] children =gameObject.GetComponentsInChildren<ElevatorDownChild>();
		foreach (ElevatorDownChild child in children){
			child.gameObject.GetComponent<BoxCollider>().enabled = false;
		}
		gameObject.GetComponent<ElevatorDown>().enabled = false;
		gameObject.GetComponent<ElevatorDown> ().t = 0;
		gameObject.transform.position = gameObject.GetComponent<ElevatorDown> ().targetB.transform.position;


	}

	void ElevatorActivated(){
		gameObject.GetComponent<ElevatorDown>().enabled = true;
		gameObject.GetComponent<ElevatorDown>().targetB.GetComponent<Collider>().enabled = false;
		player.GetComponent<playerStatus> ().floor --;
	}
}
