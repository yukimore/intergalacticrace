﻿using UnityEngine;
using System.Collections;

public class ElevatorUp : MonoBehaviour {

	public Transform targetA;
	public Transform targetB;
	public float t = 0;
	public float speed = 0.05f; // Not real speed
	
	// Update is called once per frame
	void FixedUpdate () {
		t = Mathf.Clamp (t + Time.deltaTime * speed, 0, 1);
		float sinT = (Mathf.Cos (t * Mathf.PI) + 1) / 2;
		transform.position = 
			targetA.position * (1 - sinT) +
				targetB.position * sinT;
	}

}
