﻿using UnityEngine;
using System.Collections;

public class slot : MonoBehaviour {
	
	public string key; //button to click
	public Texture notUsed = null;
	public ItemAction ButtonAction; //gets the action from key clicked
	bool clicked = false; // identif if button was already clicked with mouse to access extra inventory
	public Inventory MyInventory; 
	GameObject gameController;
	
	void Start(){
		ButtonAction = GameObject.FindGameObjectWithTag("GameController").GetComponent<ItemAction> ();
		this.GetComponent<GUITexture>().texture = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().slotsInitializer (key);
		}
	
	void Update(){
		if (Input.GetKeyDown((KeyCode)System.Enum.Parse(typeof(KeyCode), key) )){
				ButtonAction.action(this.GetComponent<GUITexture>().texture.name); //call the button functionality
			}
		}
	
	void OnMouseOver(){
		if (Input.GetMouseButtonDown(0)){
			if (clicked){ //check if slot was clicked
				textureChanged(); // delete components from extraSltos because no one was selected
				
			}else{ //add component ExtraSlotSelected to load the texture on the slot clicked;
				for (int i = 0; i < MyInventory.ExtraSlots.Length; i++){
					MyInventory.ExtraSlots[i].GetComponent<GUITexture>().enabled = true;
					MyInventory.ExtraSlots[i].gameObject.AddComponent<ExtraSlotSelected>();
					MyInventory.ExtraSlots[i].gameObject.GetComponent<ExtraSlotSelected>().slot = this;
				}
				clicked = !clicked;
			} 

			
		}
		if (Input.GetMouseButtonDown(1)){ //if click with right button destroy texture (throw object off)
			this.GetComponent<GUITexture>().texture = MyInventory.notUsed;
		}			
	}
	
	public void textureChanged(){ //called when any slot fro extra inventory was clicked
		for (int i = 0; i < MyInventory.ExtraSlots.Length; i++){
			MyInventory.ExtraSlots[i].GetComponent<GUITexture>().enabled = false;
			Destroy(MyInventory.ExtraSlots[i].GetComponent<ExtraSlotSelected>());
		}
		clicked = !clicked;
		/*for (int i = 0; i < MyInventory.ExtraSlots.Length; i++){
			Destroy(MyInventory.ExtraSlots[i].GetComponent<ExtraSlotSelected>());
		}*/

	}
}