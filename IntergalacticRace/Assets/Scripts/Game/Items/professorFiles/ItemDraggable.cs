﻿using UnityEngine;
using System.Collections;

public class ItemDraggable : Item1 {
	public float distance;

	void OnMouseDown() {
		distance = (transform.position - Camera.main.transform.position).magnitude;
		if (GetComponent<Rigidbody>() != null) {
			GetComponent<Rigidbody>().useGravity = false;
			GetComponent<Rigidbody>().velocity = Vector3.zero;
			GetComponent<Rigidbody>().detectCollisions = false;
			}
		}

	/*void OnMouseDrag() {
		isDragging = true;
		transform.position = Camera.main.ScreenPointToRay (Input.mousePosition).GetPoint (distance);
	}*/

	public virtual void OnMouseUp() {
		if (GetComponent<Rigidbody>() != null) {
			GetComponent<Rigidbody>().useGravity = true;
			GetComponent<Rigidbody>().detectCollisions = true;
			}
	}
}
