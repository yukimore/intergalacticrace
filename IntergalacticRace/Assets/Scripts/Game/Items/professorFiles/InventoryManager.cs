﻿using UnityEngine;
using System.Collections.Generic;

public class InventoryManager : MonoBehaviour {

	public List<ItemInventory> items;
	public  int pageNumber = 1;
	private bool showInventory;

	private ItemInventory selectedItem;

	// Use this for initialization
	void Start () {
		items = new List<ItemInventory> (); // create empty list
		showInventory = true;
		selectedItem = null;
	}

	/*void Update() {
				if (selectedItem != null) {
						selectedItem.transform.position = 
				Camera.main.ScreenPointToRay (Input.mousePosition).GetPoint (1);
						if (Input.GetMouseButtonUp (0)) {
								RaycastHit info;
								if (selectedItem.target != null && selectedItem.target.Raycast (
					Camera.main.ScreenPointToRay (Input.mousePosition),
					out info, Camera.main.farClipPlane)) {
										Remove (selectedItem);
										selectedItem.Use ();
										selectedItem = null;
								}
			}
				else if (Input.GetMouseButtonUp (1)) {
					selectedItem.gameObject.SetActive (false);
					selectedItem = null;
				}
						
				}
	}*/

	// We will draw our inventory as a GUI
	/*void OnGUI() {
		int itemsPerPage = 5;
		if (showInventory) {
			if(GUI.Button (new Rect (0, 0, 50, 50), "<"))
				pageNumber--;
			if(GUI.Button (new Rect ((itemsPerPage + 1) * 50, 0, 50, 50), ">"))
				pageNumber++;
			pageNumber = Mathf.Clamp(pageNumber,1, items.Count/itemsPerPage+1);
			int startIndex = (pageNumber-1) * itemsPerPage;
			for (int i = 0; i < itemsPerPage; i++)
			if(startIndex+i < items.Count) {
					if(GUI.Button (new Rect ((i+1) * 50, 0, 50, 50), 
				               items[startIndex+i].icon)) {
					Select (items[startIndex+i]);
						}
				}
				else
					GUI.Button (new Rect ((i+1) * 50, 0, 50, 50), "-");

		}
	}*/

	public void Take(ItemInventory item) {
		if (items.Contains (item))
			return;
		items.Add (item);
		item.transform.localScale *= 0.2f;
		item.gameObject.SetActive (false);
	}

	public void Remove(ItemInventory item) {
		items.Remove (item);
	}

	/*public void Select(ItemInventory item) {

		if (selectedItem != null)
						selectedItem.gameObject.SetActive (false);
		if (item.target == null) {
						Remove (item);
						item.Use ();
				} else {	
						selectedItem = item;
						selectedItem.gameObject.SetActive (true);
				}

	}*/
}
