﻿using UnityEngine;
using System.Collections;

public class bulletWallColliderBack : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Collider>().enabled = false;
		Invoke ("returnColliderWall", 5);
	}
	
	
	public void returnColliderWall(){
		GetComponent<Collider>().enabled = true;
		Destroy (this);
	}
}
