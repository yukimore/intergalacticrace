﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemAction : MonoBehaviour {

	public GameObject bullet;
	private GameObject player;
	public float speed = 20f; 

	/*public class Bullet {
		
		public string type = null;
	}*/

	void Start(){
		player = GameObject.FindGameObjectWithTag ("Player");
		}

	/* function switch case ----------------------------------------------------------------------------------------------- */
	
	// atributes
	private List<string> actions = new List<string> ();
	private List<System.Func<bool>> functions = new List<System.Func<bool>>();
	
	// constructor 
	public ItemAction () {
		actions = new List<string> ();
		functions = new List<System.Func<bool>>();
		
		actions.Add ("Gun");
		functions.Add (gun);
		
		actions.Add ("GunIce");
		functions.Add (gunIce);
		
		actions.Add ("GunFalseWall");
		functions.Add (gunFalseWall);
	}
	
	// switch case
	public void action(string action) {
		print (action);
		bullet.GetComponent<bullet> ().type = action;
		player = GameObject.FindGameObjectWithTag ("Player");
		for (int i = 0; i < actions.Count; i++) {
			if (actions[i].Equals(action)) {
				functions[i]();
			}
		}
	}
	
	/*  functions ========================================================== */
	public bool gun() {

		bullet.GetComponent<bullet> ().damage = 50f;
		GameObject b = Instantiate(bullet, player.transform.position + new Vector3 (0f, 0.5f), player.transform.rotation) as GameObject;
		b.GetComponent<Rigidbody>().velocity = player.transform.forward * speed;	
		return true;
	}
	
	public bool gunIce() {
		bullet.GetComponent<bullet> ().damage = 25f;
		GameObject b = Instantiate(bullet, player.transform.position + new Vector3 (0f, 0.5f), player.transform.rotation) as GameObject;
		b.GetComponent<Rigidbody>().velocity = player.transform.forward * speed;	
		return true;
	}
	
	public bool gunFalseWall() {
		GameObject b = Instantiate(bullet, player.transform.position + new Vector3 (0f, 0.5f), player.transform.rotation) as GameObject;
		b.GetComponent<Rigidbody>().velocity = player.transform.forward * speed;	
		return true;
	}
	
	/* end functions ======================================================= */
	
	/* end function switch case ------------------------------------------------------------------------------------------ */
	
}



