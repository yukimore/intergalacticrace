﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {

	public float damage = 0;
	public string type = "";
	public GameObject wall;
	GameObject player;

	public bullet (float damage, string type){
		this.damage = damage;
		this.type = type;

		}

	public void OnTriggerEnter(Collider colision){
		if (colision.transform.tag == "Player"){
			player = colision.gameObject;
			if (type == "Gun")
				player.GetComponent<playerStatus>().life -= 50f;
			print (player.GetComponent<playerStatus>().life);
			if (type == "GunIce")
				player.gameObject.GetComponent<Animation>().enabled = false;
		}
		if (colision.transform.tag == "wall" && type == "GunFalseWall" ){
			colision.gameObject.AddComponent<bulletWallColliderBack>();
			Destroy (gameObject);
		}


			
	}


	public void returnPlayerAnimation(){
		player.GetComponent<Animation>().enabled = true;
	}

}
