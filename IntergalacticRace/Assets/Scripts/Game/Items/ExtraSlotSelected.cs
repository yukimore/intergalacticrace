﻿using UnityEngine;
using System.Collections;

public class ExtraSlotSelected : MonoBehaviour { //goes to each extra slot in extra inventory
	
	public slot slot;
	Texture notUsed;

	void Start(){
		notUsed = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().notUsed;
	}
	
	void OnMouseDown () {
		slot.GetComponent<GUITexture>().texture = this.GetComponent<GUITexture>().texture;		
		this.GetComponent<GUITexture>().texture = notUsed;
		slot.textureChanged ();			
	}
	
}