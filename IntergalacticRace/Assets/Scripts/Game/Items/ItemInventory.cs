﻿using UnityEngine;
using System.Collections;

public class ItemInventory : MonoBehaviour {

	public string type;
	public Texture icon; // Inventory icon

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player")
			GameObject.FindGameObjectWithTag (type).GetComponent<Inventory> ().Take (this);
		}
}
