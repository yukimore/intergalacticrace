﻿using UnityEngine;
using System.Collections;

public class PauseClick : MonoBehaviour {

	public Texture actualTexture;
	public GUITexture PauseBackground;
	public GameObject activator;
	public bool actvatorOnOff;

	void OnMouseDown(){
		PauseBackground.texture = actualTexture;
		activator.SetActive (actvatorOnOff);
	}
}
