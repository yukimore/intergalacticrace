﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	
	public int[] floors;
	List<GameObject[]> allElements = new List<GameObject[]>();

	//Elevators prefab
	public GameObject elevator;
	public GameObject elevator2;
	public GameObject[] SpeedUp;

	//elements by floor
	public GameObject[] allElementsFloor01;
	public GameObject[] allElementsFloor02;
	public GameObject[] allElementsFloor03;
	public GameObject[] allElementsFloor04;
	public GameObject[] allElementsFloor05;
	public GameObject[] allElementsFloor06;
	public GameObject[] allElementsFloor07;
	public GameObject[] allElementsFloor08;

	//slots from Skills Menu
	public GameObject[]slotsMenu;

	//empty texture
	public Texture notUsed;

	void Awake(){
		//find slots from skills menu and enable off their textures
		slotsMenu = GameObject.FindGameObjectsWithTag ("slotMenu");
		foreach (GameObject slot in slotsMenu){ 
			slot.SetActive(false);
		}
	}

	void Start(){

		//delete slots from previous Menu
		Invoke ("slotsInitialized", 2);

		//adding all elements inside allEments list and placing them
		allElements.Add (allElementsFloor01);
		allElements.Add (allElementsFloor02);
		allElements.Add (allElementsFloor03);
		allElements.Add (allElementsFloor04);
		allElements.Add (allElementsFloor05);
		allElements.Add (allElementsFloor06);
		allElements.Add (allElementsFloor07);
		allElements.Add (allElementsFloor08);
		placerMultipleFloor (allElements, floors);

		//placing elevators down and up in all floors
		for (int i = 0; i < floors.Length - 1; i++) {
			gameObject.GetComponent<MazePlacer> ().setElevatorUp (elevator, floors[i]);
		}
		for (int i = 1; i < floors.Length; i++) {
			gameObject.GetComponent<MazePlacer> ().setElevatorDown (elevator2, floors [i]);
		}

		for (int i = 0; i < floors.Length; i++) {
			gameObject.GetComponent<MazePlacer> ().SetItemMaze (SpeedUp, floors [i], null);
		}		 
	}
	

	//funtcion to place all elements in the maze. objects has to come preared with all scripts and stuff
	public void placerMultipleFloor (List<GameObject[]> items, int[] floors){
		for (int i = 0; i<floors.Length; i++){
			this.GetComponent<MazePlacer>().SetItemMaze(items[i], floors[i], null);
			foreach(GameObject initialItem in items[i]){
				//Destroy(initialItem);
			}
		}
	}  

	//determining which slot gets which texture
	public Texture slotsInitializer(string slot){

		Texture slotInitialTexture = null; 
		
		switch(slot){
		case "Q":
			foreach (GameObject slotCheck in slotsMenu){
				if (slotCheck.name == "slotQ"){
					slotInitialTexture = slotCheck.GetComponent<GUITexture>().texture;
				}
			}
			break;
		case "E":
			foreach (GameObject slotCheck in slotsMenu){
				if (slotCheck.name == "slotE")
					slotInitialTexture = slotCheck.GetComponent<GUITexture>().texture;
			}
			break;
		case "R":
			foreach (GameObject slotCheck in slotsMenu){
				if (slotCheck.name == "slotR")
					slotInitialTexture = slotCheck.GetComponent<GUITexture>().texture;
			}
			break;
		case "Alpha1":
			foreach (GameObject slotCheck in slotsMenu){
				if (slotCheck.name == "slot01")
					slotInitialTexture = slotCheck.GetComponent<GUITexture>().texture;
			}
			break;

		case "Alpha2":
			foreach (GameObject slotCheck in slotsMenu){
				if (slotCheck.name == "slot02")
					slotInitialTexture = slotCheck.GetComponent<GUITexture>().texture;
			}
			break;
		case "Alpha3":
			foreach (GameObject slotCheck in slotsMenu){
				if (slotCheck.name == "slot03")
					slotInitialTexture = slotCheck.GetComponent<GUITexture>().texture;
			}
			break;			
		}

		return slotInitialTexture;
	}

	//delete skills menu slots
	public void slotsInitialized(){
		for(int i = 0; i < slotsMenu.Length; i++ ){
			Destroy (slotsMenu[i]);
		}
	}
}