﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeMaze : MonoBehaviour {
	
	public int floors;

	private Maze floor; //each floor will be a Maze object 
	private GameObject CubeMazeTransform;
	public GameObject[] CubeMazeFloors; //list of all floors transformers
	public float wallLenght = 1.0f;
	public int xSize;
	public int ySize;
	public GameObject wall; 
	private float WallHight;
	public GameObject ground;
	private GameObject Cube;


	void Start(){
		transform.tag = "CubeMaze";
		CubeMazeCreator ();
	}

	void CubeMazeCreator(){
		WallHight = wall.transform.localScale.y;
		Cube = new GameObject ();
		Cube.name = "Cube Maze";
		Vector3 initialPosition = new Vector3 (transform.position.x, -floors / 2, transform.position.z);

		CubeMazeFloors = new GameObject[floors];



		for (int i = 0; i < floors; i++){

			//defining new maze
			floor = gameObject.AddComponent<Maze>() as Maze;
			floor.xSize = xSize;
			floor.ySize = ySize;
			floor.wallLenght = wallLenght;
			floor.floorLength = wallLenght;
			floor.floor = ground;
			floor.wall = wall;

			floor.transform.position = new Vector3 (initialPosition.x, initialPosition.y + i, initialPosition.z);
			CubeMazeFloors[i] = floor.CreateLevel(i, WallHight);
			CubeMazeFloors[i].transform.parent = Cube.transform;

		}
	}
}
