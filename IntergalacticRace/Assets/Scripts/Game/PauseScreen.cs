﻿using UnityEngine;
using System.Collections;

public class PauseScreen : MonoBehaviour {

	public bool paused = false;
	public GameObject player;
	public GameObject[] pauseButtons;

	void Start(){
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			paused = !paused;
			if (paused) {
				Transform cameraTransform = Camera.main.transform;
				transform.position = cameraTransform.position;
				transform.rotation = cameraTransform.rotation;
			}
			GetComponent<Camera>().enabled = paused;
			player.SetActive (!paused);
			if (paused)
				Time.timeScale = 0;
			else
				Time.timeScale = 1;
		}
	}

	void ActivatePause(){
		foreach(GameObject pauseButton in pauseButtons){
			pauseButton.GetComponent<GUITexture>().GetComponent<Animation>().enabled = paused;
		}
	}

}
