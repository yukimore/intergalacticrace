﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Maze : MonoBehaviour {

	[System.Serializable]
	public class Cell{
		public bool visited;
		public GameObject north;
		public GameObject east;
		public GameObject west;
		public GameObject south;
	}

	public GameObject wall;
	public float wallLenght = 1.0f;
	public int xSize;
	public int ySize;
	private Vector3 initialPosition;
	private GameObject MazeTrasform;
	private Cell[] cells;
	private int currentCell = 0;
	private int totalCells;
	private int visitedCells = 0;
	private bool startedBuilding = false;
	private int currentNeighbour = 0;
	private List<int> lastCells;
	private int backingUp = 0;
	private int wallToBreak = 0;
	
	GameObject CreateWalls(float level){

		MazeTrasform = new GameObject ();
		MazeTrasform.name = "Maze";
		initialPosition = new Vector3 ((-xSize / 2) + wallLenght / 2, level, (-ySize / 2) + wallLenght / 2);
		Vector3 myPsoition = initialPosition;
		GameObject tempWall;


		for (int i = 0; i < ySize; i++) {
			for (int j = 0; j <= xSize; j++){
				myPsoition = new Vector3 (initialPosition.x + (j* wallLenght)-wallLenght, 
				                          initialPosition.y, 
				                          initialPosition.z + (i*wallLenght) - wallLenght/2);
				tempWall = Instantiate(wall, myPsoition, Quaternion.identity)as GameObject;
				tempWall.tag = "wall";
				tempWall.transform.parent = MazeTrasform.transform;
			}	
		}

		for (int i = 0; i <= ySize; i++) {
			for (int j = 0; j < xSize; j++){
				myPsoition = new Vector3 (initialPosition.x + (j* wallLenght)-wallLenght/2, 
				                          initialPosition.y, 
				                          initialPosition.z + (i*wallLenght) - wallLenght);
				tempWall = Instantiate(wall, myPsoition, Quaternion.Euler(0.0f, 90.0f, 0.0f))as GameObject;
				tempWall.tag = "wall";
				tempWall.transform.parent = MazeTrasform.transform;
			}	
		}
		CreateCells ();
		return (MazeTrasform);
	}

	void CreateCells(){
		lastCells = new List<int> ();
		lastCells.Clear ();
		totalCells = xSize * ySize;
		GameObject[] allWalls;
		int children = MazeTrasform.transform.childCount;
		allWalls = new GameObject[children];
		cells = new Cell[xSize * ySize];
		int eastWestProcess = 0;
		int childProcess = 0;
		int termCount = 0;

		//gets all the children
		for (int i = 0; i < children; i++) {
			allWalls[i] = MazeTrasform.transform.GetChild(i).gameObject;
				}

		//asign all the cells
		for (int cellProcess = 0; cellProcess < cells.Length; cellProcess++) {
			if(termCount == xSize){
				eastWestProcess++;
				termCount = 0;
			}

			cells[cellProcess] = new Cell();
			cells[cellProcess].east = allWalls[eastWestProcess];
			cells[cellProcess].south = allWalls[childProcess + (xSize+1)*ySize];

			eastWestProcess++;
		
			termCount++;
			childProcess++;
			cells[cellProcess].west = allWalls[eastWestProcess];
			cells[cellProcess].north = allWalls[(childProcess + (xSize+1)*ySize)+xSize-1];
		}
		CreateMaze ();
	}

	void CreateMaze(){
		while (visitedCells < totalCells) {
			if (startedBuilding){
				NeighbourFinder ();
				if (cells[currentNeighbour].visited == false && cells[currentCell].visited == true){
					BreakWall();
					cells[currentNeighbour].visited = true;
					visitedCells++;
					lastCells.Add(currentCell);
					currentCell = currentNeighbour;
					if (lastCells.Count > 0){
						backingUp = lastCells.Count-1;
					}
				}
			}
			else{
				currentCell = Random.Range(0, totalCells);
				cells[currentCell].visited = true;
				visitedCells++;
				startedBuilding = true;
			}
		}

		}


	void NeighbourFinder(){

		int length = 0;
		int[] neighbours = new int[4];
		int[] connectingWall = new int[4];
		int check = 0;
		check = (currentCell + 1) / xSize;
		check -= 1;
		check *= xSize;
		check += xSize;

		//west
		if (currentCell + 1 < totalCells && (currentCell + 1) != check) {
			if (cells[currentCell +1].visited == false){
				neighbours[length] = currentCell+1;
				connectingWall[length] = 3;
				length++;
			}
		}
		//east
		if (currentCell - 1 >= 0 && currentCell != check) {
			if (cells[currentCell - 1].visited == false){
				neighbours[length] = currentCell-1;
				connectingWall[length] = 2;
				length++;
			}
		}
		//north
		if (currentCell +xSize < totalCells) {
			if (cells[currentCell +xSize].visited == false){
				neighbours[length] = currentCell+xSize;
				connectingWall[length] = 1;
				length++;
			}
		}
		//south
		if (currentCell - xSize >= 0) {
			if (cells[currentCell - xSize].visited == false){
				neighbours[length] = currentCell-xSize;
				connectingWall[length] = 4;
				length++;
			}
		}

		if (length != 0) {
			int theChoseOne = Random.Range (0, length);
			currentNeighbour = neighbours[theChoseOne];
			wallToBreak = connectingWall[theChoseOne];
		}
		else{
			if (backingUp > 0){
				currentCell = lastCells[backingUp];
				backingUp--;
			}
		}
	}
	
	void BreakWall(){
		switch(wallToBreak){
		case 1: Destroy(cells[currentCell].north);
			break;
		case 2: Destroy(cells[currentCell].east);
			break;
		case 3: Destroy(cells[currentCell].west);
			break;
		case 4: Destroy(cells[currentCell].south);
			break;
		}	
	}

	public GameObject floor;
	public float floorLength;
	private GameObject MazeFloorTrasform;


	GameObject CreateFloor(float level, string floorName){
		MazeFloorTrasform = new GameObject ();
		MazeFloorTrasform.name = "Floor";
		initialPosition = new Vector3 ((-xSize / 2) + floorLength / 2, -wall.transform.localScale.y/2 + level, (-ySize / 2) + floorLength / 2);
		Vector3 myPsoition = initialPosition;
		GameObject tempFloor;
		
		for (int i = 0; i < ySize; i++) {
			for (int j = 0; j <= xSize-1; j++){
				myPsoition = new Vector3 (initialPosition.x + (j* floorLength)-floorLength/2, 
				                          initialPosition.y, 
				                          initialPosition.z + (i*floorLength) - floorLength/2);
				tempFloor = Instantiate(floor, myPsoition, Quaternion.identity)as GameObject;
				tempFloor.transform.tag = floorName; 
				tempFloor.transform.parent = MazeFloorTrasform.transform;
			}	
		}
		return (MazeFloorTrasform);
	}

	private GameObject LevelTransform; 

	public GameObject CreateLevel(int floor, float wallHight ){

		LevelTransform = new GameObject ();
		LevelTransform.name = "floor " + floor;
		GameObject tempCubeMaze;
		GameObject tempCubeMazeFloor;

		tempCubeMaze = CreateWalls(floor*wallHight);
		tempCubeMazeFloor = CreateFloor(floor*wallHight, LevelTransform.name);
		tempCubeMaze.transform.parent = LevelTransform.transform;
		tempCubeMazeFloor.transform.parent = LevelTransform.transform;

		return (LevelTransform);
	}



}
