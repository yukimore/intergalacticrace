﻿using UnityEngine;
using System.Collections;

public class PuzzleController : MonoBehaviour {

	public Texture[] rightSequence;
	public GameObject[] slots;
	public int condition;

	public GameObject slotKey;
	public Texture key;

	public GUITexture finalPopUp;
	public Texture withoutkey;
	public Texture withKey;

	public GameObject[] ObjectKey;
	
	// Update is called once per frame
	public void verify () {
		for (int i = 0; i < rightSequence.Length; i++){
			if (rightSequence[i] == slots[i].GetComponent<GUITexture>().texture){
				condition++;
				if (condition == 8){
					GoFindKey();
					break;
				}				
			}else{
				condition = 0;
				break;
			}
		}

		if (slotKey.GetComponent<GUITexture>().texture == key){
			finalPopUp.texture = withKey;
			Invoke("GoFinalBossMaze", 5);
		}
	}

	public void GoFindKey(){
		finalPopUp.texture = withoutkey;
		finalPopUp.enabled = true;
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		gameObject.GetComponent<MazePlacer> ().SetItemMaze (ObjectKey,player.GetComponent<playerStatus> ().floor, null);
	}

	public void GoFinalBossMaze(){
		Application.LoadLevel ("VictoryRoom");
	}
}
