﻿using UnityEngine;
using System.Collections;

public class ItemInventoryPuzzle : MonoBehaviour {

	public string type = "puzzle";
	public Texture icon; // Inventory icon

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player")
			GameObject.FindGameObjectWithTag (type).GetComponent<InventoryPuzzle> ().Take (this);
		}
}
