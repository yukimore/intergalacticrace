﻿using UnityEngine;
using System.Collections;

public class ExtraSlotSelectedPuzzle : MonoBehaviour { //goes to each extra slot in extra inventory
	
	public slotPuzzle slot;

	void OnMouseDown () {
		slot.GetComponent<GUITexture>().texture = this.GetComponent<GUITexture>().texture;		
		slot.textureChanged ();			
	}
	
}