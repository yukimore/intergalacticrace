﻿using UnityEngine;
using System.Collections;

public class slotPuzzle : MonoBehaviour {

	public InventoryPuzzle MyInventory;
	bool clicked = false; // identif if button was already clicked with mouse to access extra inventory
	
	void OnMouseOver(){
		if (Input.GetMouseButtonDown(0)){
			
			if (clicked){ //check if slot was clicked
				textureChanged(); // delete components from extraSltos because no one was selected
				
			}else{ //add component ExtraSlotSelected to load the texture on the slot clicked;
				for (int i = 0; i < MyInventory.ExtraSlots.Length; i++){
					MyInventory.ExtraSlots[i].GetComponent<GUITexture>().enabled = true;
					MyInventory.ExtraSlots[i].gameObject.AddComponent<ExtraSlotSelectedPuzzle>();
					MyInventory.ExtraSlots[i].gameObject.GetComponent<ExtraSlotSelectedPuzzle>().slot = this;
					clicked = !clicked;
				}
			} 

			
		}
		if (Input.GetMouseButtonDown(1)){ //if click with right button destroy texture (throw object off)
			this.GetComponent<GUITexture>().texture = MyInventory.notUsed;
		}			
	}
	
	public void textureChanged(){ //called when any slot fro extra inventory was clicked
		for (int i = 0; i < MyInventory.ExtraSlots.Length; i++){
			MyInventory.ExtraSlots[i].GetComponent<GUITexture>().enabled = false;
			Destroy(MyInventory.ExtraSlots[i].GetComponent<ExtraSlotSelectedPuzzle>());
		}
		GameObject.FindGameObjectWithTag ("GameController").GetComponent<PuzzleController> ().verify ();
		clicked = !clicked;
	}
}