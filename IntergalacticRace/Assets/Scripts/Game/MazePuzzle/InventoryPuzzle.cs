﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryPuzzle : MonoBehaviour {

	public GameObject[] slots; //usable slots
	public GameObject[] ExtraSlots; //inventory where items go
	public Texture notUsed; //without functionality


	void Start () {
	
		for (int i = 0; i<slots.Length; i++){ //gets the item selected on the previos menu
			slots[i].AddComponent<slotPuzzle>();
			slots[i].GetComponent<slotPuzzle>().MyInventory = this;
		}

		for (int i= 0; i < ExtraSlots.Length; i++){
			ExtraSlots[i].GetComponent<GUITexture>().enabled = false;
		}
	}
	
	public void Take(ItemInventoryPuzzle item){
		for (int i = 0; i < ExtraSlots.Length; i++){
			if (ExtraSlots[i].GetComponent<GUITexture>().texture == notUsed){
				ExtraSlots[i].GetComponent<GUITexture>().texture = item.icon;
				Destroy(item.gameObject);
				break;
			}
		}
	}


}
