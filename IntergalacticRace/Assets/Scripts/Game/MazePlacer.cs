﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MazePlacer : MonoBehaviour {
	
	private GameObject[] floorList;

	public void SetItemMaze (GameObject[] items, int floor, string questTag){

		floorList = GameObject.FindGameObjectsWithTag ("floor " + floor);

		for (int i = 0; i < items.Length; i++){
			int place = Random.Range(0, floorList.Length);
			Vector3 myPosition = floorList[place].transform.position;
			if (questTag != null)
				items[i].tag = questTag;
			Instantiate(items[i], new Vector3(myPosition.x, myPosition.y+0.5f, myPosition.z),Quaternion.identity);
		}
	}

	public void setElevatorUp(GameObject elevator, int floor){

		floorList = GameObject.FindGameObjectsWithTag ("floor " + floor);
		GameObject[] floorList02 = GameObject.FindGameObjectsWithTag ("floor " + (floor + 1));

		int place = Random.Range(0, floorList.Length);
		Vector3 targetA = floorList[place].transform.position;
		GameObject elevatorUp = Instantiate(elevator, targetA,Quaternion.identity) as GameObject;
		elevatorUp.GetComponent<ElevatorUp> ().targetB = floorList[place].transform;
		elevatorUp.GetComponent<ElevatorUp> ().targetA = floorList02[place].transform;

	}
	public void setElevatorDown(GameObject elevator, int floor){
		
		floorList = GameObject.FindGameObjectsWithTag ("floor " + floor);
		GameObject[] floorList02 = GameObject.FindGameObjectsWithTag ("floor " + (floor -1));
		
		int place = Random.Range(0, floorList.Length);
		Vector3 targetB = floorList[place].transform.position;
		GameObject elevatorDown = Instantiate(elevator, targetB,Quaternion.identity) as GameObject;
		elevatorDown.GetComponent<ElevatorDown> ().targetB = floorList[place].transform;
		elevatorDown.GetComponent<ElevatorDown> ().targetA = floorList02[place].transform;
		
	}
}

