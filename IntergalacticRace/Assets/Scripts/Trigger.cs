﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour {

	public GameObject shooter;
	public GameObject bullet;
	public GameObject direction;
	public float time;
	public bool timer;

	// Use this for initialization
	void Start () {
		timer = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (timer) {
			time += Time.deltaTime;
			shooter.transform.LookAt (direction.transform.position);
			if (time >= 1) {
				time --;
				((GameObject)Instantiate (bullet, shooter.transform.position, shooter.transform.rotation)).GetComponent<Bullet>().shot(direction);
			}
		}
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag.Equals ("Player")) {
			direction = other.gameObject;
			timer = true;
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag.Equals ("Player")) {
			timer = false;
			time = 0;
			direction = null;
		}
	}
}
