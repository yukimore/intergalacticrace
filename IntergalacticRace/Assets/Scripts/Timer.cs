﻿using UnityEngine;
using System.Collections;
using System;

public class Timer : MonoBehaviour {

	public Vector2 position;
	public int minutes;
	public float seconds;
	private string text;

	// Use this for initialization
	void Start () {
		//position = new Vector2 (10, 10);
	}
	
	// Update is called once per frame
	void Update () {

		text = String.Format("{0:00}:{1:00}",minutes, (int)seconds);
		seconds -= Time.deltaTime;

		if (seconds <= 0) {
			if (minutes > 0) {
				seconds = 60;
				minutes--;
			}

			if (minutes == 0 && seconds <= 0) {
				Application.LoadLevel("OpeningMenu");
			}
		}
	}

	void OnGUI() {
		GUI.Box (new Rect (position.x, position.y, 50, 25), text);
	}
}
