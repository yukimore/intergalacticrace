﻿using UnityEngine;
using System.Collections;

public class SkillChoicePopUp : MonoBehaviour {

	public GameObject[] slotsOption;
	public Texture[] gunsAndPowers;
	public Texture[] Guns;
	public Texture[] Powers;
	public Texture[] UsableItems;
	public Texture unknown;
	//public string slot;
	//public SkillChoiceSlot slotChoice;
	
	public void PopUpCreation (string slot) {

		
		if  (slot == "slotQ"){
				for (int i = 0; i<slotsOption.Length; i++){
				if (i<Guns.Length)
					slotsOption[i].GetComponent<GUITexture>().texture = Guns[i];

					else
					slotsOption[i].GetComponent<GUITexture>().texture = unknown;
				}
		}

		if (slot == "slotE"){
			for (int i = 0; i<slotsOption.Length; i++){
				if (i<gunsAndPowers.Length)
					slotsOption[i].GetComponent<GUITexture>().texture = gunsAndPowers[i];
				else
					slotsOption[i].GetComponent<GUITexture>().texture = unknown;
			}
		}
		
		if (slot == "slotR"){
			for (int i = 0; i<slotsOption.Length; i++){
				if (i<Powers.Length)
					slotsOption[i].GetComponent<GUITexture>().texture = Powers[i];
				else
					slotsOption[i].GetComponent<GUITexture>().texture = unknown;
			}
		}
		
		else{
			for (int i = 0; i<slotsOption.Length; i++){
				if (i<UsableItems.Length)
					slotsOption[i].GetComponent<GUITexture>().texture = UsableItems[i];
				else
					slotsOption[i].GetComponent<GUITexture>().texture = unknown;
			}
		}

		//slotChoice.created ();

	}

	
}

