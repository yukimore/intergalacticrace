﻿using UnityEngine;
using System.Collections;

public class SlotGroup : MonoBehaviour {

	// Use this for initialization
	public GameObject skillsPopUp;
	public GameObject skillsController;
	public bool popUpVisible = false;
	public GameObject selectedSlot;

	void Awake(){
		DontDestroyOnLoad (transform.gameObject);
	}
	
	void Update () {
		skillsPopUp.SetActive (popUpVisible);
	
	}

}
