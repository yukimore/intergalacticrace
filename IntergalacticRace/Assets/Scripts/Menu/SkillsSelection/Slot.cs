﻿using UnityEngine;
using System.Collections;

public class Slot : MonoBehaviour {

	// Use this for initialization
	string name;					//slot name (activation key: Q, E, R, 1, 2 or 3)
	GameObject skillsPopUp;			//object that parents all options in the popUp
	GameObject skillsController;	//scene controller


	void Start () {
		name = this.transform.name;
		skillsPopUp = transform.parent.GetComponent<SlotGroup> ().skillsPopUp;
		skillsController = transform.parent.GetComponent<SlotGroup> ().skillsController;
	}


	void OnMouseUpAsButton (){
		skillsController.GetComponent<SkillsController> ().PopUp (name);	//Send this slot name to create the popUp
		if (!transform.parent.GetComponent<SlotGroup> ().popUpVisible)		//if popUp isn't visible, turn it visible
			transform.parent.GetComponent<SlotGroup> ().popUpVisible = true;
		transform.parent.GetComponent<SlotGroup> ().selectedSlot = this.gameObject;	//indicates that this was the clicked slot
	}
}
