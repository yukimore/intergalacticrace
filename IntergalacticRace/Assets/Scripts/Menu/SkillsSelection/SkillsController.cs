﻿using UnityEngine;
using System.Collections;

public class SkillsController : MonoBehaviour {

	// Use this for initialization
	public GameObject[] AvailableOption;	//all popUp options available
	public Texture[] GunsAndPowers;			//guns and power-ups list
	public Texture[] Guns;					
	public Texture[] Powers;				//power-ups for guns
	public Texture[] UsableItems;			//items
	public Texture Unknown;					//"Not-Used" texture  
	public GameObject[] Slots;				
	public GameObject slotGroup;			

	void Start () {
		foreach (GameObject availableOption in AvailableOption){
			availableOption.GetComponent<GUITexture>().texture=Unknown;		//start popUp slots with "Not-Used" texure
			availableOption.AddComponent<Option>().slotGroup = slotGroup;	//add "Option" script and assimilates slotGroup on it
		}
		foreach (GameObject slots in Slots){
			slots.GetComponent<GUITexture>().texture=Unknown;				//start slots with Not-Used" texture
		}
	}

	//build the popUp based on the clicked slot
	public void PopUp (string slotName){		

		switch(slotName){

		case "slotQ":
			for (int i = 0; i<AvailableOption.Length; i++){
				if (i<Guns.Length)
					AvailableOption[i].GetComponent<GUITexture>().texture = Guns[i];
				
				else
					AvailableOption[i].GetComponent<GUITexture>().texture = Unknown;
			}
			break;

		case "slotE":
			for (int i = 0; i<AvailableOption.Length; i++){
				if (i<GunsAndPowers.Length)
					AvailableOption[i].GetComponent<GUITexture>().texture = GunsAndPowers[i];
				else
					AvailableOption[i].GetComponent<GUITexture>().texture = Unknown;
			}
			break;

		case "slotR":
			for (int i = 0; i<AvailableOption.Length; i++){
				if (i<Powers.Length)
					AvailableOption[i].GetComponent<GUITexture>().texture = Powers[i];
				else
					AvailableOption[i].GetComponent<GUITexture>().texture = Unknown;
			}
			break;

		default:
			for (int i = 0; i<AvailableOption.Length; i++){
				if (i<UsableItems.Length)
					AvailableOption[i].GetComponent<GUITexture>().texture = UsableItems[i];
				else
					AvailableOption[i].GetComponent<GUITexture>().texture = Unknown;
			}
			break;
		}
	}
}
