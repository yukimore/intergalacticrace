﻿using UnityEngine;
using System.Collections;

public class LevelText : MonoBehaviour {

	public GUIText levelInfo;

	void Start(){
		levelInfo.text = "Lurin, the representative of Earth, is about to start his journey to keep our\n" +
						"planet safe. In his first challenge, Lurin has to conquer the Cube Maze, a 3\n" +
						"dimensional maze there is very trick. Lurin has to find the eight pieces of \n" +
						"the main map that will give him the exact position where to go to find the \n" +
						"boss room. There, Lurin will have to solve the boss puzzle, a sequence game \n" +
						"that he has to repeat every time the boss do it. The hard part is the boss \n" +
						"keeps changing the sequence, which means that the next sequence will not \n" +
						"start as the previous one.\n" +
						"\n" +
						"Your challenge is to help Lurin to solve all the things that come up to him.\n" +
						"You have the power to choose one gun, one hiper power, and another slot so \n" +
						"you can select whatever you want. Also, you can pick initially one item to \n" +
						"start the Game. Will you be able to help Lurin and save our planet?";
	}
}
