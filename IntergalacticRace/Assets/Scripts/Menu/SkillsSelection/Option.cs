﻿using UnityEngine;
using System.Collections;

public class Option : MonoBehaviour {

	// Use this for initialization
	public GameObject slotGroup; //parent of slots
	
	//changes the selected slot's texture to this texture
	void OnMouseUpAsButton () {	
	slotGroup.GetComponent<SlotGroup> ().selectedSlot.GetComponent<GUITexture> ().texture = 
			this.transform.GetComponent<GUITexture> ().texture;
		slotGroup.GetComponent<SlotGroup> ().popUpVisible = false;
	}
}
